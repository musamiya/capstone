#!/usr/bin/env python

from wtforms import StringField, SubmitField, IntegerField
from wtforms.validators import DataRequired, NumberRange
from flask_wtf import Form


class SymbolSearch(Form):
	"""
	This class builds input forms for user's GUI and input validators
	"""
	ticker_name = StringField('Ticker', validators=[DataRequired("Please enter a valid ticker")])

	ma_short = IntegerField('Fast Moving Average',
	                        validators=[
		                        NumberRange(min=5, message="Fast moving average has to be bigger than 5"),
		                        DataRequired("Moving average is required")
	                        ])

	ma_long = IntegerField('Slow Moving Average',
	                       validators=[
		                       NumberRange(min=10, message="Slow moving average has to be bigger than 10"),
		                       DataRequired("Moving average is required")
	                       ])
	submit = SubmitField('Run')
