#!/usr/bin/env python

from flask import Flask, request, render_template, redirect, url_for, flash
from classForms import SymbolSearch as ss

import classCerebro as cb
import datetime

import pandas_datareader.data as web
import pandas as pd

from plotly.tools import FigureFactory as FF
import plotly.graph_objs as go
import plotly.plotly as ply


app = Flask(__name__)

ply.sign_in('username', 'key')
app.config.from_pyfile("config.cfg")

@app.route("/")
def index():
	"""
	The welcoming page
	:return: page template
	"""
	return render_template("index.html")


@app.route("/about")
def about():
	"""
	Details about the project
	:return: page template
	"""
	return render_template("about.html")


@app.route("/service", methods=['GET', 'POST'])
def service():
	"""
	This page takes several inputs from the user and passes the parameters to the results method
	:return: page template
	"""
	form = ss(csrf_enabled=False)

	# first validate the input using WTForms
	if request.method == 'POST' and form.validate():

		# make sure that the Long(Slow) MA is bigger than the Short(Fast) MA
		if int(request.form['ma_long']) < int(request.form['ma_short']):
			flash("Fast MA should be smaller than slow MA")

			return render_template('service.html', form=form)
		else:
			return redirect(url_for('results', ticker_name=request.form['ticker_name'],
			                        ma_short=int(request.form['ma_short']),
			                        ma_long=int(request.form['ma_long'])))

	return render_template('service.html', form=form)


@app.route("/symbol/<ticker_name>,<int:ma_short>,<int:ma_long>")
def results(ticker_name, ma_short, ma_long):
	"""
	Rendering the stock with moving average crossover.
	Due to a bug in the annotations, they are not cleared on a second run of the method

	:param ticker_name: string
	:param ma_short:    int
	:param ma_long:     int
	:return:            template
	"""

	# TODO - this is no longer needed here: remove and perform a unit test
	if ma_short < 5:
		flash("Short MA has to be at least 5")
		return redirect(url_for('service'))

	# TODO - this is no longer needed here: remove and perform a unit test
	if ma_long < 10:
		flash("Long MA has to be at least 10")
		return redirect(url_for('service'))

	# set the start and end date for retrieving data
	# 1 year back from the current date
	today = datetime.datetime.now()
	start = datetime.datetime(today.year - 1, today.month, today.day)

	# in case the user inputs a non existing ticker
	try:
		data = web.DataReader(ticker_name, 'yahoo', start, today)
	except Exception as e:
		flash("That ticker does not exist")
		return redirect(url_for('service'))

	# user's choice of strategy
	current = cb.classCerebro()
	current.setSymbol(ticker_name)

	current.setStrategy(int(ma_short), int(ma_long))
	current_portfolio_value = current.getPortfolioValue()

	signals = current.getSignals()

	# optimal strategy
	optimal = current.setOptimalStrategy(int(ma_short), int(ma_long))
	optimal_portfolio_value = optimal['portfolio_value']
	optimal_smafast = optimal['smafast']
	optimal_smaslow = optimal['smaslow']

	# plotting the data
	data['Fast Moving Average'] = pd.rolling_mean(data['Adj Close'], window=int(ma_short))
	data['Slow Moving Average'] = pd.rolling_mean(data['Adj Close'], window=int(ma_long))

	# the graph
	fig = FF.create_candlestick(data.Open, data.High, data.Low, data.Close, dates=data.index)

	up_line = FF.create_candlestick(data.Open, data.High, data.Low, data.Close, dates=data.index,
	                                direction='increasing', fillcolor="white",
	                                line=go.Line(color="rgb(0,0,255)", width=.5))

	down_line = FF.create_candlestick(data.Open, data.High, data.Low, data.Close, dates=data.index, name="Downtrend",
	                                  direction='decreasing', fillcolor="white",
	                                  line=go.Line(color="rgb(255,0,0)", width=.5))

	ma_short_line = go.Scatter(x=data.index, y=data['Fast Moving Average'], name='Fast Moving Average',
	                           line=go.Line(color="rgb(249, 203, 156)", shape="spline", dash="dot"))

	ma_long_line = go.Scatter(x=data.index, y=data['Slow Moving Average'], name='Slow Moving Average',
	                          line=go.Line(color="rgb(182, 215, 168)", shape="spline", dash="dot"))

	fig['data'].extend([ma_short_line, ma_long_line])
	fig['data'].extend(up_line['data'])
	fig['data'].extend(down_line['data'])

	# unfortunately the annotations are not cleaned at this point (BUG???)
	fig['layout']['annotations'] = []
	for i in signals:
		fig['layout']['annotations'].append(go.Annotation(i))
	# fig['layout'].append(title='MA Crossover Strategy for ' + ticker_name, annotations=signals)

	# plot the data
	plot = ply.plot(fig, auto_open=False, showLink=False, validate=False)
	table = data.tail().to_html()

	# the template
	return render_template('results.html', symbol=ticker_name, table=table, plot=plot,
	                       ma_short=ma_short, ma_long=ma_long, current_portfolio_value=current_portfolio_value,
	                       optimal_smafast=optimal_smafast, optimal_smaslow=optimal_smaslow,
	                       optimal_portfolio_value=optimal_portfolio_value)


if __name__ == '__main__':
	# app.run(debug=True, port=9999)    # don't use debug=True in production
	app.run()
