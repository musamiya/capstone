#!/usr/bin/env python

from __future__ import (absolute_import, division, print_function, unicode_literals)
import backtrader as bt


class StrategyMovingAverageCrossover(bt.Strategy):
	"""
	This class builds a simple moving average crossover strategy
	"""

	"""
	Default parameters for
	stake:              10 - the number of units used for each transaction
	smafast:            15 - the number of days to calculate the short(fast) moving average
	smaslow:            30 - the number of days to calculate the long(slow) moving average
	printlog:           False - boolean to activate verbose mode
	portfolio_value:    0 - the value of the portfolio before initialization
	"""
	params = (('stake', 10), ('smafast', 15), ('smaslow', 30), ('printlog', False), ('portfolio_value', 0))

	# buy/sel signals list
	signals = []

	def log(self, txt, dt=None, doprint=False):
		"""
		Helper function for logging activity
		:param txt:         String
		:param dt:          datetime
		:param doprint:     Boolean
		:return:            IO
		"""
		if self.params.printlog or doprint:
			dt = dt or self.datas[0].datetime.date(0)
			print("%s, %s" % (dt.isoformat(), txt))


	def signal(self, color, price, signal, date=None):
		"""
		Retrieve buy/sell signals
		:param color:   String (red for sell / green for buy)
		:param price:   Int
		:param signal:  String
		:param date:    datetime
		:return:        list
		"""
		date = date or self.datas[0].datetime.date(0)
		self.signals.append((date, price, color, signal))


	def __init__(self):
		# Keep a reference to the "close" line in the data[0] dataseries
		self.dataclose = self.datas[0].close

		# set the size stake from parameters
		self.sizer.setsizing(self.params.stake)

		self.order = None       # track pending transactions
		self.buyprice = None    # buying price
		self.buycomm = None     # commission

		# adding moving average indicator
		sma_fast = bt.indicators.MovingAverageSimple(self.datas[0], period=self.params.smafast)
		sma_slow = bt.indicators.MovingAverageSimple(self.datas[0], period=self.params.smaslow)

		# the signal
		self.buysignal = bt.indicators.CrossOver(sma_fast, sma_slow)


	def notify_order(self, order):
		"""
		Execute an order of buy or sell if there are no pending orders
		:param order:   Order
		:return:        None
		"""

		# make sure we are not in an ongoing transaction
		if order.status in [order.Submitted, order.Accepted]:
			return

		# check if an order has been completed (if not enough cash, the order will fail)
		if order.status in [order.Completed, order.Canceled, order.Margin]:
			if order.isbuy():
				self.log("BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm: %.2f" %
				         (order.executed.price, order.executed.value, order.executed.comm))

				self.buyprice = order.executed.price
				self.buycomm = order.executed.price


			elif order.issell():
				self.log("SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm: %.2f" %
				         (order.executed.price, order.executed.value, order.executed.comm))


			self.bar_executed = len(self)

		# no pending order
		self.order = None


	def notify_trade(self, trade):
		"""
		Executed after each order.
		Calculate the value of the transaction and the commission
		:param trade:   Trade
		:return:        None
		"""
		if not trade.isclosed:
			return

		self.log("OPERATION PROFIT, GROSS %.2f, NET %.2f" % (trade.pnl, trade.pnlcomm))


	def next(self):
		"""
		This method is executed for each unit
		:return:    None
		"""
		# Simply log the closing price of the series from the reference
		self.log('Close Price, %.2f' % self.dataclose[0])

		# check for pending orders
		if self.order:
			return

		# check if we are in the market
		if not self.position:
			# SMA - simple moving average strategy
			if self.buysignal > 0:
				self.log("BUY CREATE, %.2f" % self.dataclose[0])
				self.signal(self.dataclose[0], str("rgb(0,255,0)"), str("Buy"))
				self.order = self.buy()

		else:
			# SMA - simple moving average strategy
			if self.buysignal < 0:
				self.log("SELL CREATE, %.2f" % self.dataclose[0])
				self.signal(self.dataclose[0], str("rgb(255,0,0)"), str("Sell"))
				self.order = self.sell()


	def stop(self):
		"""
		Used when running strategy optimization
		this method collects all the strategies and their value for the envelope range
		:return:
		"""
		self.log("MA Crossover results {value: %.2f, fast: %2d, slow: %2d}" %
		         (self.broker.getvalue(), self.params.smafast, self.params.smaslow), doprint=False)
		self.params.portfolio_value = self.broker.getvalue()
