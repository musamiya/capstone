#About the project


- This system is a stock recommendation system based on the moving average crossover strategy.
The user will be presented with the option of choosing a listed company and two moving
averages *fast* and *slow*.


- The starting amount of cash will be **10000 USD** and each trade will be performed with a fix amount
of **10 stock units**.
The system will backtest the moving average strategy for the previous year and present the user with
the final  value of its portfolio, *cash* and *stocks*.


- In order to make the trading simulation as close as possible to the real world, the system will have
a rate of **.1% fees** from each successful transaction.


- Based on user's choice of strategy and equity, the model will optimize the parameters of the strategy
(*namely the values of the moving averages*) in order to maximize the returns.
The recommendation will be presented to the user as a comparison between user's own choice
and the system's optimized choice.
