#!/usr/bin/env python

from __future__ import (absolute_import, division, print_function, unicode_literals)
from strategies import StrategyMovingAverageCrossover
import backtrader as bt

import datetime
import pandas_datareader.data as web
import pandas as pd


class classCerebro(object):
	"""
	This class serves as interface between the GUI and the backend
	"""
	def __init__(self):
		self.symbol = ""            # tickers
		self.curr = bt.Cerebro()    # user's strategy
		self.opt = bt.Cerebro()     # optimized strategy
		self.signals = {}           # buy/sell signals

	# getters
	def getSymbol(self):
		"""
		Retrieve the active ticker
		:return:    String
		"""
		return self.symbol

	def getPortfolioValue(self):
		"""
		The current value of the portfolio
		:return:    Int
		"""
		return self.curr.broker.getvalue()

	def getSignals(self):
		"""
		Buy/Sell signals
		This is used to plot the annotations on the chart
		:return:
		"""
		keys    = [str("x"), str("arrowcolor"), str("y"), str("text")]
		extra   = {str("arrowhead"): 2, str("arrowwidth"): 4.5, str("ax"): 0, str("ay"): 30}

		# get the signals from the strategy and make it a data frame
		df = pd.DataFrame(StrategyMovingAverageCrossover.signals, columns=keys).to_dict(orient="records")

		# create a list of dictionaries
		self.signals = [dict(x, **extra) for x in df]

		return self.signals

	# setters

	def tsData(self):
		"""
		Using pandas to extract the data
		:return:    data frame
		"""

		# set the start and end date for retrieving data
		# 1 year back from the current date
		today = datetime.datetime.now()
		start = datetime.datetime(today.year - 1, today.month, today.day)

		# Yahoo APIs
		panda_data = web.DataReader(self.getSymbol(), 'yahoo', start, today)

		# transform the data in a format for backtrader
		data = bt.feeds.PandasData(dataname=panda_data)
		# data = bt.feeds.YahooFinanceCSVData(dataname="MSFT.txt")

		return data

	def setSymbol(self, newSymbol):
		"""
		Set the symbol(ticker)
		:param newSymbol:
		:return:    String
		"""
		self.symbol = newSymbol

	def setStrategy(self, newSMAfast, newSMAslow):
		"""
		Set the fast(short) MA and the slow(long) MA
		We also set 10,000 USD as starting capital
		The commission is 0.1%
		Every transaction is fixed to 10 units

		:param newSMAfast:  Int
		:param newSMAslow:  Int
		:return:            Strategy object
		"""

		# only MA crossover strategy will be available
		self.curr.addstrategy(StrategyMovingAverageCrossover,
		                         smafast=newSMAfast,
		                         smaslow=newSMAslow,
		                         stake=10,
		                         printlog=False)

		# feed the data series
		self.curr.adddata(self.tsData())

		# hardcoded start cash and commission values
		self.curr.broker.setcash(10000)
		self.curr.broker.setcommission(.001)

		# run the current strategy
		self.curr.run()

	def setOptimalStrategy(self, newSMAfast, newSMAslow):
		"""
		Same parameters as setStrategy method
		The return will be a dictionary containing parameters for the optimal strategy

		:param newSMAfast:  Int
		:param newSMAslow:  Int
		:return:            Dictionary
		"""
		self.opt.optstrategy(StrategyMovingAverageCrossover,
		                     # the MA envelope is hardcoded - TODO create a better way to set the envelope
		                         smafast=xrange(newSMAfast - 4, newSMAfast + 6),
		                         smaslow=xrange(newSMAslow - 4, newSMAslow + 6),
		                         stake=10,
		                         printlog=False)

		self.opt.adddata(self.tsData())

		# hardcoded start cash and comission values
		self.opt.broker.setcash(10000)
		self.opt.broker.setcommission(.001)

		optimization = self.opt.run()

		# retrieve all the strategies within the envelope
		strategies = [x[0] for x in optimization]

		# sort the strategies by the portfolio vaue
		strategies.sort(key=lambda strategy: strategy.params.portfolio_value, reverse=True)

		# get teh best strategy
		best = strategies[0].params._getkwargs()

		return best

